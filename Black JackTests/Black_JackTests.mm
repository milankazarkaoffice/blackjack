//
//  Black_JackTests.m
//  Black JackTests
//
//  Created by Milan Kazarka on 3/1/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CBjLogic.hpp"

@interface Black_JackTests : XCTestCase {
CBjLogic *logicInstance;
}

@end

@implementation Black_JackTests

- (void)setUp {
    [super setUp];
    logicInstance = &CBjLogic::getInstance();
    logicInstance->newGamePlayable();
}

- (void)tearDown {
    [super tearDown];
}

- (void)testGameReady {
    XCTAssertTrue(logicInstance->getState()==_GAME_STATE_PLAYABLE);
}

- (void)testCardsPlayerReady {
    XCTAssertTrue(logicInstance->player.count==2);
}

- (void)testCardsHouseReady {
    XCTAssertTrue(logicInstance->house.count==2);
}

- (void)testCardsHit {
    logicInstance->hit();
    XCTAssertTrue(logicInstance->player.count>2);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        NSLog(@"start");
        for(int n = 0; n < 250; n++) {
            logicInstance->newGamePlayable();
            while(logicInstance->hit()==_GAME_STATE_PLAYABLE) { }
        }
        NSLog(@"finish");
    }];
}

@end
