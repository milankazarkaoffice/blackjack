//
//  bjActor.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "bjActor.h"
#import "bjCard.h"
#import "bjCommon.h"

@implementation bjActor

-(id)init {
    self = [super init];
    if (self) {
        self.cards = [[NSMutableOrderedSet alloc] init];
        self.sum = 0;
    }
    return self;
}

-(id)initWithSaveStateDef:(NSDictionary*)saveStateDef {
    self = [super init];
    if (self) {
        self.cards = [[NSMutableOrderedSet alloc] init];
        NSArray *cards = [saveStateDef objectForKey:@"cards"];
        for(NSDictionary *cardDef in cards) {
            bjCard *card = [[bjCard alloc] initWithCardDef:cardDef typeStr:[cardDef objectForKey:@"typeStr"]];
            bjCard *cardSetCard = nil;
            for(cardSetCard in [bjCommon shared].cardsSet) {
                if ([cardSetCard.typeStr isEqualToString:card.typeStr] &&
                    cardSetCard.value==card.value) {
                    break;
                }
            }
            if (cardSetCard) {
                [[bjCommon shared].cardsSet removeObject:cardSetCard];
                [[bjCommon shared].cardsSet addObject:card];
                BOOL visible = card.visible;
                card.drawn = YES;
                card.visible = visible;
            } else {
                // assert
            }
            [self.cards addObject:card];
        }
        self.sum = 0;
    }
    return self;
}

-(void)reset {
    // seems double - we also reset on the all cards level
    for(bjCard *card in self.cards)
        card.drawn = NO;
    [self.cards removeAllObjects];
}

-(BOOL)getCard:(NSObject<CardProvider>*)provider {
    if (!provider)
        return NO;
    [self.cards addObject:(bjCard*)[provider getCard]];
    return YES;
}

-(BOOL)getInitialCards:(NSObject<CardProvider>*)provider canOverdraft:(BOOL)canOverdraft {
    if (!provider)
        return NO;
    for(int n = 0; n < 2; n++) {
        [self getCard:provider];
    }
    if (!canOverdraft) {
        if (self.sum>=21) {
            [self reset];
            [self getInitialCards:provider canOverdraft:canOverdraft];
        }
    }
    return YES;
}

-(CARDVALUE)sum {
    unsigned int elevens = 0;
    CARDVALUE completesum = 0;
    for(bjCard *card in self.cards) {
        if (card.value==11)
            elevens++;
        if (card.visible)
            completesum+=card.value;
    }
    while(completesum>MAGICNUMBER && elevens>0) {
        completesum-=10;
        elevens--;
    }
    _sum = completesum;
    return _sum;
}

-(NSMutableDictionary*)actorDef {
    NSMutableDictionary *actorDef = [[NSMutableDictionary alloc] init];
    NSMutableArray *cardsArray = [[NSMutableArray alloc] init];
    [actorDef setObject:cardsArray forKey:@"cards"];
    for(bjCard *card in self.cards) {
        [cardsArray addObject:card.cardDef];
    }
    return actorDef;
}

@end
