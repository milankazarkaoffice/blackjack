//
//  bjCommon.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "bjCommon.h"
#import "bjCard.h"

@interface bjCommon ()

@property (strong,nonatomic) NSArray<NSString*> *types;

@end

@implementation bjCommon

+(bjCommon*)shared {
    static bjCommon *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[bjCommon alloc] init];
    });
    return sharedInstance;
}

-(NSArray<NSString*>*)types {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _types = @[@"c",@"d",@"h",@"s"];
    });
    return _types;
}

/** for security reasons these things shouldn't be in .plists / elsewhere in monetized games
 */
-(NSArray<NSDictionary*>*)cardTypeDef {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _cardTypeDef = @[@{@"val":@2,@"strId":[NSNull null]},
                         @{@"val":@3,@"strId":[NSNull null]},
                         @{@"val":@4,@"strId":[NSNull null]},
                         @{@"val":@5,@"strId":[NSNull null]},
                         @{@"val":@6,@"strId":[NSNull null]},
                         @{@"val":@7,@"strId":[NSNull null]},
                         @{@"val":@8,@"strId":[NSNull null]},
                         @{@"val":@9,@"strId":[NSNull null]},
                         @{@"val":@10,@"strId":[NSNull null]},
                         @{@"val":@10,@"strId":@"j"},
                         @{@"val":@10,@"strId":@"q"},
                         @{@"val":@10,@"strId":@"k"},
                         @{@"val":@11,@"strId":@"a"}
                 ];
    });
    return _cardTypeDef;
}

-(bjCard*)randomCard {
    // could be also an ordered Set, than we don't need "allObjects"
    return [[self.cardsSet allObjects] objectAtIndex:arc4random_uniform((uint32_t)self.cardsSet.count)];
}

-(NSMutableSet<bjCard*>*)cardsSet {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _cardsSet = [[NSMutableSet alloc] init];
        for(NSString *type in self.types) {
            for(NSDictionary* cardDef in self.cardTypeDef) {
                bjCard *card = [[bjCard alloc] initWithCardDef:cardDef typeStr:type];
                [_cardsSet addObject:card];
            }
        }
    });
    return _cardsSet;
}

-(Card*)getCard {
    bjCard *card = nil;
    while((card = [bjCommon shared].randomCard)) {
        if (card.drawn)
            continue;
        card.drawn = YES;
        return card;
    }
    return nil;
}

-(void)resetCards {
    for(bjCard *card in self.cardsSet) {
        [card reset];
    }
}

@end
