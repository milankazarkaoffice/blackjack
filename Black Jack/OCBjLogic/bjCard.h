//
//  bjCard.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "Card.h"

@interface bjCard : Card

@property (strong,nonatomic) NSString *typeStr;
@property (strong,nonatomic) NSMutableDictionary *cardDef;
@property (strong,nonatomic) NSString *filename;

-(id)initWithCardDef:(NSDictionary*)cardDef typeStr:(NSString*)typeStr;

@end
