//
//  bjActorPlayer.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "bjActorPlayer.h"

@implementation bjActorPlayer

-(id)init {
    self = [super init];
    if (self) {
        self.stand = NO;
    }
    return self;
}

-(id)initWithSaveStateDef:(NSDictionary*)saveStateDef {
    self = [super initWithSaveStateDef:saveStateDef];
    if (self) {
        NSNumber *standBoolNum = [saveStateDef objectForKey:@"stand"];
        if (standBoolNum) {
            self.stand = standBoolNum.boolValue;
        }
    }
    return self;
}

-(void)reset {
    [super reset];
    self.stand = NO;
}

-(NSMutableDictionary*)actorDef {
    NSMutableDictionary *def = [super actorDef];
    [def setObject:[NSNumber numberWithBool:self.stand] forKey:@"stand"];
    return def;
}

@end
