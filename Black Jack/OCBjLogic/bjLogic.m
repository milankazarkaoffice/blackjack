//
//  bjLogic.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "bjLogic.h"
#import "bjCommon.h"
#import "bjCard.h"
#import "bjActorPlayer.h"
#import <objc/runtime.h>

@interface bjLogic ()
@end

@implementation bjLogic

+(bjLogic*)shared {
    static bjLogic *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[bjLogic alloc] init];
    });
    return sharedInstance;
}

-(id)init {
    self = [super init];
    if (self) {
        if (![self loadSavedGame]) {
            self.house = [[bjActor alloc] init];
            self.player = [[bjActorPlayer alloc] init];
            _state = GameStateGeneric;
        }
    }
    return self;
}

-(void)reset {
    [[bjCommon shared] resetCards];
    [self.house reset];
    [self.player reset];
}

-(NSError*)newGame {
    NSLog(@"bjLogic newGame");
    if (self.state==GameStatePlayable)
        return nil;
    [self reset];
    [self.house getInitialCards:[bjCommon shared] canOverdraft:YES];
    [self.player getInitialCards:[bjCommon shared] canOverdraft:NO];
    
    if (self.house.cards.count==2 && self.player.cards.count==2) {
        _state = GameStatePlayable;
        [self.house.cards objectAtIndex:1].visible = NO;
        return nil;
    }
    return [NSError errorWithDomain:@"Couldn't initialize game" code:2 userInfo:nil];
}

-(void)printState {
    NSLog(@"    house sum(%ld)",(unsigned long)self.house.sum);
    NSLog(@"    player sum(%ld)",(unsigned long)self.player.sum);
    if (self.state) { }
}

-(GameState)getStateFromSums {
    if (self.house.sum==self.player.sum && self.house.sum>=14)
        return GameStateStandoff;
    else if (self.house.sum==MAGICNUMBER) {
        return GameStateHouseWins;
    } else if (self.player.sum==MAGICNUMBER) {
        return GameStatePlayerWins;
    } else {
        // strage that abs complains about a value being unsigned where as the result is what can be negative, not the input value
        if (self.house.sum>=14) {
            if (self.house.sum>MAGICNUMBER)
                return GameStatePlayerWins;
            if (abs((int)self.player.sum-MAGICNUMBER)<abs((int)self.house.sum-MAGICNUMBER))
                return GameStatePlayerWins;
            else
                return GameStateHouseWins;
        }
    }
    return _state;
}

-(GameState)state {
    if (self.player.stand) {
        if (self.house.sum>=14)
            _state = [self getStateFromSums];
    } else {
        if (self.player.sum>MAGICNUMBER)
            _state = GameStateHouseWins;
        else
            _state = [self getStateFromSums];
    }
    
    switch(_state) {
        case GameStateHouseWins:
            NSLog(@"    House Wins");
            break;
        case GameStatePlayerWins:
            NSLog(@"    Player Wins");
            break;
        case GameStateStandoff:
            NSLog(@"    Standoff");
            break;
        case GameStatePlayable:
            NSLog(@"    Playable");
            break;
        case GameStateGeneric:
            NSLog(@"    Game in generic state");
            break;
            // XCode tells us if we miss a type
    }
    
    return _state;
}

-(GameState)hit {
    [self.player getCard:[bjCommon shared]];
    return self.state;
}

-(GameState)stand {
    self.player.stand = YES;
    if ([self.house.cards objectAtIndex:1].visible==NO)
        [self.house.cards objectAtIndex:1].visible = YES;
    else
        [self.house getCard:[bjCommon shared]];
    return self.state;
}

-(NSString*)stateName {
    if ([bjLogic shared].state==GameStateStandoff)
        return @"Standoff";
    else if ([bjLogic shared].state==GameStatePlayerWins)
        return @"Player wins";
    else if ([bjLogic shared].state==GameStateHouseWins)
        return @"House wins";
    return @"";
}

/** in theory this should be saved differently in monetized games - basically rather to the keychain than user defaults, but for the sake of the test let's do it this way
 */
-(BOOL)save {
    NSLog(@"save");
    if (self.state==GameStatePlayable) {
        NSMutableDictionary *saveState = [[NSMutableDictionary alloc] init];
        [saveState setObject:[self.house actorDef] forKey:@"house"];
        [saveState setObject:[self.player actorDef] forKey:@"player"];
        NSData *data=[NSKeyedArchiver archivedDataWithRootObject:saveState];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"saveState"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"saveState"]) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"saveState"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    return YES;
}

-(BOOL)loadSavedGame {
    NSData *saveStateData = [[NSUserDefaults standardUserDefaults] objectForKey:@"saveState"];
    if (saveStateData) {
        NSDictionary *saveState = [NSKeyedUnarchiver unarchiveObjectWithData:saveStateData];
        _state = GameStatePlayable;
        NSDictionary *houseDef = [saveState objectForKey:@"house"];
        self.house = [[bjActor alloc] initWithSaveStateDef:houseDef];
        NSDictionary *playerDef = [saveState objectForKey:@"player"];
        self.player = [[bjActorPlayer alloc] initWithSaveStateDef:playerDef];
        return YES;
    }
    return NO;
}

@end
