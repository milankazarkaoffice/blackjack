//
//  bjCard.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "bjCard.h"

@implementation bjCard

@synthesize cardDef = _cardDef;

-(id)initWithCardDef:(NSDictionary*)cardDef typeStr:(NSString*)typeStr {
    self = [super init];
    if (self) {
        self.cardDef = [[NSMutableDictionary alloc] initWithDictionary:cardDef];
        self.typeStr = typeStr;
    }
    return self;
}

-(void)setCardDef:(NSMutableDictionary *)cardDef {
    _cardDef = cardDef;
    self.value = (CARDVALUE)[[_cardDef objectForKey:@"val"] unsignedIntegerValue]; // just in case if the CARDVALUE would be changed, could be done differently
    NSNumber *visibleBoolNum = [_cardDef objectForKey:@"visible"];
    if (visibleBoolNum)
        self.visible = visibleBoolNum.boolValue;
}

/** for recreation purposes when saving / reloading of game state
 */
-(NSMutableDictionary*)cardDef {
    NSMutableDictionary *copy = [[NSMutableDictionary alloc] initWithDictionary:_cardDef];
    [copy setObject:[NSNumber numberWithBool:self.visible] forKey:@"visible"];
    [copy setObject:self.typeStr forKey:@"typeStr"];
    return copy;
}

-(NSString*)filename {
    NSString *valueSign = nil;
    if ([self.cardDef objectForKey:@"strId"]==[NSNull null])
        valueSign = [NSString stringWithFormat:@"%ld",(unsigned long)self.value];
    else
        valueSign = [self.cardDef objectForKey:@"strId"];
    return [NSString stringWithFormat:@"%@%@.png",self.typeStr,valueSign];
}

@end
