//
//  bjActor.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "CardProvider.h"
#import "Primitives.h"

@class bjCard;

@interface bjActor : NSObject {
}

@property (strong,nonatomic) NSMutableOrderedSet<bjCard*> *cards;
@property (assign,nonatomic) CARDVALUE sum; // could be "unsigned char"
@property (strong,nonatomic) NSMutableDictionary *actorDef;

-(id)initWithSaveStateDef:(NSDictionary*)saveStateDef;
-(void)reset;
-(BOOL)getCard:(NSObject<CardProvider>*)provider;
-(BOOL)getInitialCards:(NSObject<CardProvider>*)provider canOverdraft:(BOOL)canOverdraft; // overdraft could also be class variable from the getgo

@end
