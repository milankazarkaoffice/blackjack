//
//  CardProvider.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#ifndef CardProvider_h
#define CardProvider_h

#import <Foundation/Foundation.h>

@class Card;

@protocol CardProvider <NSObject>
-(Card*)getCard;
@end

#endif /* CardProvider_h */
