//
//  bjCommon.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#ifndef bjCommon_h
#define bjCommon_h

#define MAGICNUMBER 21

#import "CardProvider.h"

typedef NS_ENUM(NSUInteger, GameState) {
    GameStateGeneric,
    GameStatePlayable,
    GameStateHouseWins,
    GameStatePlayerWins,
    GameStateStandoff
};

@class bjCard;

@interface bjCommon : NSObject <CardProvider>

@property (strong,nonatomic) bjCard *randomCard;
@property (strong,nonatomic) NSArray<NSDictionary*> *cardTypeDef; // one of 4 type defs
@property (strong,nonatomic) NSMutableSet<bjCard*> *cardsSet; // complete cards set

+(bjCommon*)shared;
-(void)resetCards; // could be in provider

@end

#endif /* bjCommon_h */
