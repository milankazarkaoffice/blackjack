//
//  Card.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Primitives.h"

@interface Card : NSObject

@property (assign,nonatomic) CARDVALUE value;
@property (assign,nonatomic) BOOL drawn;
@property (assign,nonatomic) BOOL visible;

-(void)reset;

@end
