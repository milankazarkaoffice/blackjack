//
//  bjActorPlayer.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "bjActor.h"

@interface bjActorPlayer : bjActor

@property (assign,nonatomic) BOOL stand;

@end
