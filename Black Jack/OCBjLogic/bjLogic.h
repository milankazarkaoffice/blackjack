//
//  bjLogic.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "bjCommon.h"

@class bjActor;
@class bjActorPlayer;

@interface bjLogic : NSObject

@property (strong,nonatomic) bjActor *house;
@property (strong,nonatomic) bjActorPlayer *player;
@property (assign,nonatomic) GameState state;
@property (strong,nonatomic) NSString *stateName;

+(bjLogic*)shared;
-(NSError*)newGame;
-(void)printState;
-(GameState)hit;
-(GameState)stand;
-(BOOL)save;

@end
