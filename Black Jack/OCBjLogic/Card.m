//
//  Card.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "Card.h"

@implementation Card

-(id)init {
    self = [super init];
    if (self) {
        self.value = 0;
        [self reset];
    }
    return self;
}

-(void)setDrawn:(BOOL)drawn {
    _drawn = drawn;
    if (_drawn)
        self.visible = YES;
}

-(void)reset {
    self.drawn = NO;
    self.visible = NO;
}

@end
