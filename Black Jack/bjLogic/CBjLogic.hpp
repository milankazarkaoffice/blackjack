//
//  CBjLogic.hpp
//  Black Jack
//
//  Created by Milan Kazarka on 2/28/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

/**
 
    the idea behind the game logic being written in C++ is that you can port it to
    anything including the toaster. We did this back in Vysoko anime production and
    the old game logic engine survived the test of time.
 
 */

#ifndef CBjLogic_hpp
#define CBjLogic_hpp

#include <stdio.h>

typedef unsigned char NUMBER;

static NUMBER type_set[13] = {  2,3,4,
                                5,6,7,
                                8,9,10,
                                12,13,14,11};

enum type {
    _TYPE_C = 0,
    _TYPE_D,
    _TYPE_H,
    _TYPE_S
};

enum gameState {
    _GAME_STATE_DORMANT = 0,
    _GAME_STATE_PLAYABLE,
    _GAME_STATE_HOUSE_WIN,
    _GAME_STATE_PLAYER_WIN,
    _GAME_STATE_STANDOFF
};

struct s_card {
    bool drawn;
    bool visible;
    NUMBER value;
    NUMBER type;
};

struct s_actor {
    s_card **cards;
    NUMBER count;
};

NUMBER cardsCount(s_card *cards[]);

class CBjLogic {
private:
    
    CBjLogic(CBjLogic const&);
    void operator=(CBjLogic const&);
    
    s_card cards[52];
    NUMBER state;
    
    bool newGame( );
    void resetCards( );
    s_card *getCard( );
    void resetActor( s_actor &actor );
    void debugActor( s_actor &actor );
    void addCard( s_actor &actor );
public:
    s_actor house;
    s_actor player;
    
    static CBjLogic& getInstance( ) {
        static CBjLogic    instance;
        return instance;
    }
    
    CBjLogic( );
    void newGamePlayable( );
    NUMBER hit( );
    NUMBER stand( );
    bool canStand( );
    NUMBER getState( );
    void debugState( );
    bool playerWinRelative( );
    NUMBER actorSum( s_actor &actor );
};

#endif /* CBjLogic_hpp */
