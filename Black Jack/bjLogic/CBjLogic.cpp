//
//  CBjLogic.cpp
//  Black Jack
//
//  Created by Milan Kazarka on 2/28/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#include "CBjLogic.hpp"
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))
#define PERFECTSUM 21

CBjLogic::CBjLogic( ) {
    state = _GAME_STATE_DORMANT;
    house.cards = nullptr;
    house.count = 0;
    player.cards = nullptr;
    player.count = 0;
    srand( static_cast<unsigned int>(time(NULL))); // should only be called once
    for(NUMBER n = _TYPE_C; n < _TYPE_S+1; n++) {
        for(NUMBER ntype = 0; ntype < sizeof(type_set); ntype++) {
            cards[(n*sizeof(type_set))+ntype].type = n;
            cards[(n*sizeof(type_set))+ntype].value = type_set[ntype];
        }
    }
    printf("check order\n");
}

void CBjLogic::debugState( ) {
    printf("player:\n");
    debugActor(player);
    printf("house:\n");
    debugActor(house);
    NUMBER state = getState();
    if (state==_GAME_STATE_PLAYER_WIN) {
        printf("    PLAYER WIN\n");
    } else if (state==_GAME_STATE_HOUSE_WIN) {
        printf("    HOUSE WIN\n");
    } else if (state==_GAME_STATE_STANDOFF) {
        printf("    STANDOFF\n");
    }
}

void CBjLogic::resetCards( ) {
    for(NUMBER n = 0; n < NELEMS(cards); n++) {
        cards[n].drawn = false;
        cards[n].visible = true;
    }
}

inline int random(int min, int max){
    return min + rand() / (RAND_MAX / (max - min + 1) + 1);
}

s_card *CBjLogic::getCard( ) {
    
    bool bnone = true;
    for(NUMBER n = 0; n < NELEMS(cards); n++)
        if (!cards[n].drawn) bnone = false;
    if (bnone) return nullptr;
    
    while(s_card *card = &cards[random(0, NELEMS(cards)-1)]) {
        if (!card->drawn) {
            card->drawn = true;
            return card;
        }
    }
    return nullptr;
}

bool CBjLogic::newGame( ) {
    printf("\nnewGame()\n");
    resetCards();
    resetActor(house);
    resetActor(player);
    house.cards = (s_card**)malloc(sizeof(s_card*)*2);
    for(NUMBER n = 0; n < 2; n++) {
        addCard(house);
        if (n==1)
            house.cards[house.count-1]->visible = false;
        printf("house card(%d)  type(%d) value(%d)\n",
               n,house.cards[n]->type,house.cards[n]->value);
    }
    if (actorSum(house)==PERFECTSUM) // we don't allow a straight up win
        return false;
    player.cards = (s_card**)malloc(sizeof(s_card*)*2);
    for(NUMBER n = 0; n < 2; n++) {
        addCard(player);
        printf("player card(%d) type(%d) value(%d)\n",
               n,player.cards[n]->type,player.cards[n]->value);
    }
    if (actorSum(player)==PERFECTSUM)
        return false;
    state = _GAME_STATE_PLAYABLE;
    return true;
}

/** we don't allow the initial cards to be the PERFECTSUM for either
    with this we dissallow the player to immediately WIN at a new game launch
 */
void CBjLogic::newGamePlayable( ) {
    while(1) {
        if (!newGame()) continue;
        else break;
    }
}

void CBjLogic::resetActor( s_actor &actor ) {
    if (actor.cards) {
        free(actor.cards);
        actor.cards = nullptr;
    }
    actor.count = 0;
}

void CBjLogic::addCard( s_actor &actor ) {
    actor.count++;
    actor.cards = (s_card**)realloc(actor.cards, sizeof(s_card*)*actor.count);
    actor.cards[actor.count-1] = getCard();
}

void CBjLogic::debugActor( s_actor &actor ) {
    NUMBER sum = 0;
    for(NUMBER n = 0; n < actor.count; n++) {
        printf("    actor card(%d)  type(%d) value(%d)\n",
               n,actor.cards[n]->type,actor.cards[n]->value);
        sum+=actor.cards[n]->value;
    }
    printf("    sum(%d)\n",actorSum(actor));
}

/** if the sum is above the PERFECTSUM than we lower the worth of 11 to 1
    until it is bellow or at 21
 */
NUMBER CBjLogic::actorSum( s_actor &actor ) {
    NUMBER sum = 0;
    NUMBER elevens = 0;
    NUMBER value = 0;
    for(NUMBER n = 0; n < actor.count; n++) {
        if (actor.cards[n]->visible) {
            value = actor.cards[n]->value;
            if (value>11&&value<15)
                value=10; // J,Q,K
            sum+=value;
            if (actor.cards[n]->value==11)
                elevens++;
        }
    }
    while(sum>PERFECTSUM && elevens>0) {
        sum-=10;
        elevens--;
    }
    if (sum>30) {
        // #todo - assert, sanity check, can't have a sum above 30
    }
    return sum;
}

NUMBER CBjLogic::hit( ) {
    printf("hit\n");
    addCard(player);
    return getState();
}

/** house hits if his sum is under 14
 */
NUMBER CBjLogic::stand( ) {
    printf("stand\n");
    if (canStand()) {
        if (!house.cards[1]->visible)
            house.cards[1]->visible = true;
        else
            addCard(house);
    } else {
        if (playerWinRelative())
            state = _GAME_STATE_PLAYER_WIN;
        else {
            if (actorSum(player)==actorSum(house))
                state = _GAME_STATE_STANDOFF;
            else
                state = _GAME_STATE_HOUSE_WIN;
        }
    }
    return getState();
}

bool CBjLogic::canStand( ) {
    if (actorSum(house)>=14)
        return false;
    return true;
}

/** calculates who is closer to the PERFECTSUM
 */
bool CBjLogic::playerWinRelative( ) {
    if (abs(actorSum(player)-PERFECTSUM)<abs(actorSum(house)-PERFECTSUM))
        return true;
    return false;
}

/** calculate state
    #todo - this should actually have all the logic and it shouldn't be
    split between hit() stand() and here
 */
NUMBER CBjLogic::getState( ) {
    if (state==_GAME_STATE_PLAYABLE) {
        if (actorSum(player)==PERFECTSUM) {
            state = _GAME_STATE_PLAYER_WIN;
        } else if (actorSum(player)>PERFECTSUM) {
            state = _GAME_STATE_HOUSE_WIN;
        } else {
            if (actorSum(house)==PERFECTSUM)
                state = _GAME_STATE_HOUSE_WIN;
            else if (actorSum(house)>PERFECTSUM)
                state = _GAME_STATE_PLAYER_WIN;
        }
    }
    return state;
}
