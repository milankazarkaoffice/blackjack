//
//  SceneViewController.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/1/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneViewController : UIViewController

@property (weak,nonatomic) IBOutlet UIButton *hitBtn;
@property (weak,nonatomic) IBOutlet UIButton *standBtn;
@property (weak,nonatomic) IBOutlet UIView *houseCardsHolder;
@property (weak,nonatomic) IBOutlet UIView *playerCardsHolder;
@property (weak,nonatomic) IBOutlet UILabel *houseScoreLabel;
@property (weak,nonatomic) IBOutlet UILabel *playerScoreLabel;
@property (weak,nonatomic) IBOutlet UIView *stackHolder;
@property (strong,nonatomic) NSMutableArray *houseCardViews;
@property (strong,nonatomic) NSMutableArray *playerCardViews;

-(IBAction)onHit;
-(IBAction)onStand;
-(void)newGame;

@end
