//
//  OCCoreSceneViewController.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "OCCoreSceneViewController.h"
#import "bjLogic.h"
#import "bjActorPlayer.h"
#import "OCCardView.h"

@interface OCCoreSceneViewController ()
@property (strong,nonatomic) NSString *stateTitle;
@end

@implementation OCCoreSceneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    [self newGame];
}

-(void)newGame {
    [self removeCardArray:self.houseCardViews];
    [self removeCardArray:self.playerCardViews];
    [[bjLogic shared] newGame];
    [[bjLogic shared] printState];
    [self update];
}

-(IBAction)onHit {
    NSLog(@"OCCoreSceneViewController onHit");
    [[bjLogic shared] hit];
    [[bjLogic shared] printState];
    [self update];
}

-(IBAction)onStand {
    NSLog(@"OCCoreSceneViewController onStand");
    [[bjLogic shared] stand];
    [[bjLogic shared] printState];
    [self update];
}

-(void)update {
    self.houseScoreLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)[bjLogic shared].house.sum];
    self.playerScoreLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)[bjLogic shared].player.sum];
    if ([bjLogic shared].state!=GameStatePlayable) {
        [self onEndGame];
    }
    for(OCCardView *cv in self.houseCardViews)
        [cv onUpdate];
    [self placeActorCards:[bjLogic shared].house holderView:self.houseCardsHolder array:self.houseCardViews];
    
    for(OCCardView *cv in self.playerCardViews)
        [cv onUpdate];
    [self placeActorCards:(bjActor*)[bjLogic shared].player holderView:self.playerCardsHolder array:self.playerCardViews];
}

-(OCCardView*)cardViewForCard:(bjCard*)card array:(NSMutableArray*)array {
    if (!card)
        return nil;
    for(OCCardView *cv in array) {
        if (cv.card==card)
            return cv;
    }
    return nil;
}

-(void)removeCardArray:(NSMutableArray*)array {
    for(UIView *v in array)
        [v removeFromSuperview];
    [array removeAllObjects];
}

-(void)placeActorCards:(bjActor*)actor holderView:(UIView*)holderView array:(NSMutableArray*)views {
    int n = 0;
    for(bjCard *card in actor.cards) {
        OCCardView *cv = [self cardViewForCard:card array:views];
        if (!cv) {
            cv = [[OCCardView alloc] initWithFrame:self.stackHolder.frame];
            cv.card = card;
            [views addObject:cv];
            [self.view addSubview:cv];
            
            [UIView animateWithDuration:0.18
                                  delay:0.0
                                options:0
                             animations:^{
                                 cv.frame = CGRectMake((n*40.0f)+holderView.frame.origin.x,holderView.frame.origin.y+10.0f,100.0f,holderView.bounds.size.height);
                             } completion:^(BOOL finished) {
                                 [cv removeFromSuperview];
                                 cv.frame = CGRectMake(n*40.0f,10.0f,100.0f,holderView.bounds.size.height);
                                 [holderView addSubview:cv];
                                 if ([bjLogic shared].player.stand) {
                                     if ([bjLogic shared].state==GameStatePlayable)
                                         [[bjLogic shared] stand];
                                 }
                                 [self placeActorCards:actor holderView:holderView array:views];
                             }];
            return;
        } else {
            if ([bjLogic shared].player.stand && [bjLogic shared].state==GameStatePlayable) {
                [self onStand];
                return;
            }
        }
        n++;
    }
}

-(NSString*)stateTitle {
    return [bjLogic shared].stateName;
}

-(void)onEndGame {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:[NSString stringWithFormat:@"%@ h(%ld):p(%ld)",self.stateTitle,(unsigned long)[bjLogic shared].house.sum,(unsigned long)[bjLogic shared].player.sum]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [self newGame];
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
