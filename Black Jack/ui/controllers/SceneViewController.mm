//
//  SceneViewController.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/1/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "SceneViewController.h"
#import "CBjLogic.hpp"
#import "CardView.hh"

@interface SceneViewController ()
@property (strong,nonatomic) NSString *stateTitle;

@end

@implementation SceneViewController

- (BOOL)prefersStatusBarHidden {
    return YES; // casino - no clock or mirrors 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hitBtn.layer.cornerRadius = 10.0f;
    self.standBtn.layer.cornerRadius = 10.0f;
    self.houseCardViews = [[NSMutableArray alloc] init];
    self.playerCardViews = [[NSMutableArray alloc] init];
    self.houseCardsHolder.layer.cornerRadius = 10.0f;
    self.playerCardsHolder.layer.cornerRadius = 10.0f;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self newGame];
}

-(void)newGame {
    [self removeCardArray:self.houseCardViews];
    [self removeCardArray:self.playerCardViews];
    CBjLogic::getInstance().newGamePlayable();
    [self update];
}

-(IBAction)onHit {
    [self onState:CBjLogic::getInstance().hit()];
}

-(IBAction)onStand {
    [self onState:CBjLogic::getInstance().stand()];
}

-(NSString*)stateTitle {
    if (CBjLogic::getInstance().getState()==_GAME_STATE_STANDOFF)
        _stateTitle = @"Standoff";
    else if (CBjLogic::getInstance().getState()==_GAME_STATE_PLAYER_WIN)
        _stateTitle = @"Player wins";
    else if (CBjLogic::getInstance().getState()==_GAME_STATE_HOUSE_WIN)
        _stateTitle = @"House wins";
    return _stateTitle;
}

-(void)onState:(NUMBER)state {
    [self debugState];
    [self update];
    if (state!=_GAME_STATE_PLAYABLE) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:self.stateTitle
                                     message:@""
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [self newGame];
                                    }];
        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)debugState {
    CBjLogic::getInstance().debugState();
}

-(void)removeCardArray:(NSMutableArray*)array {
    for(UIView *v in array)
        [v removeFromSuperview];
    [array removeAllObjects];
}

-(CardView*)cardViewForCard:(s_card*)card array:(NSMutableArray*)array {
    if (!card)
        return nil;
    for(CardView *cv in array) {
        if (cv->mcard==card)
            return cv;
    }
    return nil;
}

-(void)placeActorCards:(s_actor*)actor holderView:(UIView*)holderView array:(NSMutableArray*)views {
    for(NUMBER n = 0; n < actor->count; n++) {
        CardView *cv = [self cardViewForCard:actor->cards[n] array:views];
        if (!cv) {
            cv = [[CardView alloc] initWithFrame:self.stackHolder.frame];
            [cv setCard:actor->cards[n]];
            [views addObject:cv];
            [self.view addSubview:cv];
            
            [UIView animateWithDuration:0.15
                                  delay:0.0
                                options:0
                             animations:^{
                                 cv.frame = CGRectMake((n*40.0f)+holderView.frame.origin.x,holderView.frame.origin.y+10.0f,100.0f,holderView.bounds.size.height);
                             } completion:^(BOOL finished) {
                                 [cv removeFromSuperview];
                                 cv.frame = CGRectMake(n*40.0f,10.0f,100.0f,holderView.bounds.size.height);
                                 [holderView addSubview:cv];
                                 [self placeActorCards:actor holderView:holderView array:views];
                             }];
            return;
        }
    }
}

-(void)update {
    
    for(CardView *cv in self.houseCardViews)
        [cv onUpdate];
    [self placeActorCards:&CBjLogic::getInstance().house holderView:self.houseCardsHolder array:self.houseCardViews];
    
    for(CardView *cv in self.playerCardViews)
        [cv onUpdate];
    [self placeActorCards:&CBjLogic::getInstance().player holderView:self.playerCardsHolder array:self.playerCardViews];
    
    self.houseScoreLabel.text = [NSString stringWithFormat:@"%d",CBjLogic::getInstance().actorSum(CBjLogic::getInstance().house)];
    self.playerScoreLabel.text = [NSString stringWithFormat:@"%d",CBjLogic::getInstance().actorSum(CBjLogic::getInstance().player)];
}

@end
