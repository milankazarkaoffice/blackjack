//
//  CardView.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/1/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "CardView.hh"

@implementation CardView

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self->mcard = nullptr;
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.imageView];
    }
    return self;
}

-(UIImage*)imageForCard:(s_card*)card {
    NSString *strType = @"";
    NSString *strValue = nil;
    if (card->value>10&&card->value<15) {
        if (card->value==11)        strValue = @"a";
        else if (card->value==12)   strValue = @"j";
        else if (card->value==13)   strValue = @"q";
        else if (card->value==14)   strValue = @"k";
    } else {
        strValue = [NSString stringWithFormat:@"%d",card->value];
    }
    if (card->type==_TYPE_C) strType = @"c";
    else if (card->type==_TYPE_D) strType = @"d";
    else if (card->type==_TYPE_H) strType = @"h";
    else if (card->type==_TYPE_S) strType = @"s";
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@%@.png",strType,strValue]];
}

-(void)onUpdate {
    if (mcard->visible || CBjLogic::getInstance().getState() != _GAME_STATE_PLAYABLE)
        self.imageView.image = [self imageForCard:mcard];
    else
        self.imageView.image = [UIImage imageNamed:@"back.png"];
}

-(void)setCard:(s_card*)card {
    if (!card)
        return;
    mcard = card;
    [self onUpdate];
}

@end
