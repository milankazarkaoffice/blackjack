//
//  OCCardView.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "bjCard.h"

@interface OCCardView : UIView

@property (strong,nonatomic) UIImageView *imageView;
@property (strong,nonatomic) bjCard *card;

-(void)onUpdate;

@end
