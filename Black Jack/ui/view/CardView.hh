//
//  CardView.h
//  Black Jack
//
//  Created by Milan Kazarka on 3/1/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBjLogic.hpp"

@interface CardView : UIView {
@public
    s_card *mcard;
}

@property (strong,nonatomic) UIImageView *imageView;

-(void)setCard:(s_card*)card;
-(void)onUpdate;

@end
