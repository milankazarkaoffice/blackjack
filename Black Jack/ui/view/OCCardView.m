//
//  OCCardView.m
//  Black Jack
//
//  Created by Milan Kazarka on 3/3/18.
//  Copyright © 2018 Rendering Pixels Limited. All rights reserved.
//

#import "OCCardView.h"

@implementation OCCardView

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.card = nil;
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.imageView];
    }
    return self;
}

-(void)setCard:(bjCard *)card {
    _card = card;
    [self onUpdate];
}

-(void)onUpdate {
    if (self.card) {
        if (self.card.visible)
            self.imageView.image = [UIImage imageNamed:self.card.filename];
        else
            self.imageView.image = [UIImage imageNamed:@"back.png"];
    }
}

@end
